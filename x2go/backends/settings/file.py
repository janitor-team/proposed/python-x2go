# -*- coding: utf-8 -*-

# Copyright (C) 2010-2020 by Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
#
# Python X2Go is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# Python X2Go is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program; if not, write to the
# Free Software Foundation, Inc.,
# 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

"""\
X2GoClientSettings class - managing x2goclient settings file.

The :class:`x2go.backends.settings.file.X2GoClientSettings` class one of Python X2Go's a public API classes.
Use this class (indirectly by retrieving it from an :class:`x2go.client.X2GoClient` instance)
in your Python X2Go based applications to access the
»settings« configuration file of your X2Go client application.

This class supports reading the »settings« configuration from a file (``~/.x2goclient/settings``).

"""
__NAME__ = 'x2gosettings-pylib'

__package__ = 'x2go.backends.settings'
__name__    = 'x2go.backends.settings.file'

# Python X2Go modules
import x2go.log as log
from x2go.defaults import X2GO_SETTINGS_CONFIGFILES as _X2GO_SETTINGS_CONFIGFILES
from x2go.defaults import X2GO_CLIENTSETTINGS_DEFAULTS as _X2GO_CLIENTSETTINGS_DEFAULTS
import x2go.inifiles as inifiles


class X2GoClientSettings(inifiles.X2GoIniFile):
    """\
    Configuration file based settings for :class:`x2go.client.X2GoClient` instances.


    """
    def __init__(self, config_files=_X2GO_SETTINGS_CONFIGFILES, defaults=_X2GO_CLIENTSETTINGS_DEFAULTS, logger=None, loglevel=log.loglevel_DEFAULT):
        """\
        Constructs an :class:`x2go.backends.settings.file.X2GoClientSettings` instance. This is normally done from within an :class:`x2go.client.X2GoClient` instance.
        You can retrieve this :class:`x2go.backends.settings.file.X2GoClientSettings` instance with the :func:`X2GoClient.get_client_settings() <x2go.client.X2GoClient.get_client_settings()>`
        method.

        On construction the :class:`x2go.backends.settings.file.X2GoClientSettings` object is filled with values from the configuration files::

            /etc/x2goclient/settings
            ~/.x2goclient/settings

        The files are read in the specified order and config options of both files are merged. Options
        set in the user configuration file (``~/.x2goclient/settings``) override global options set in
        ``/etc/x2goclient/settings``.

        """
        inifiles.X2GoIniFile.__init__(self, config_files, defaults=defaults, logger=logger, loglevel=loglevel)
