x2go.backends.info package
==========================

Submodules
----------

.. toctree::

   x2go.backends.info.plain

Module contents
---------------

.. automodule:: x2go.backends.info
    :members:
    :undoc-members:
    :show-inheritance:
